# Verwenden Sie ein offizielles Python-Runtime-Image als Eltern-Image
FROM python:3.10

# Installieren Sie die für Vulkan erforderlichen Pakete
RUN apt-get update && apt-get install -y --no-install-recommends \
    libvulkan1 \
    libvulkan-dev \
    vulkan-tools \
    && rm -rf /var/lib/apt/lists/*


# Setzen Sie die Umgebungsvariablen für Vulkan und Nvidia
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility

# Installieren Sie die für Vulkan erforderlichen Pakete
RUN pip install gpt4all

CMD ["bash"]
